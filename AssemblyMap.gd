extends Spatial

class_name AssemblyMap

var _game_state: GameState

var BlockDefault = load("res://blocks/BlockDefault.gd")
var ItemDefault = load("res://items/ItemDefault.gd")


class BlockDataMap:
	extends Object
	var _map: Dictionary = {}
	var _items: Dictionary = {}

	func _init():
		_map = {}
		_items = {}

	func hit_test(points, offset: Vector2 = Vector2.ZERO):
		for point in points:
			if _map.has(point + offset):
				return true
		return false

	func get_block(point: Vector2):
		return _map.get(point, null)

	func add_block(block):
		for point in block._get_occupied_points():
			if _map.has(point):
				print("warning: block is colliding with an existing block at ", point)
				break
		_map[block.block_position] = block

	func remove_block(block):
		for point in block._get_occupied_points():
			return _map.erase(point)

	func get_item(point: Vector2):
		return _items.get(point, null)

	func add_item(item):
		if _items.has(item.item_position):
			print("warning: item was added to an occupied item position")
		_items[item.item_position] = item

	func remove_item(item):
		return _items.erase(item.item_position)

	func clear():
		_map = {}
		_items = {}


var _block_hit_map: BlockDataMap

export var bounds: Rect2
export var build_bounds: Rect2


func _ready():
	_game_state = get_node("/root/GameState")

	_game_state.connect("sim_start", self, "_on_sim_start")
	_game_state.connect("sim_end", self, "_on_sim_end")
	_game_state.connect("tick", self, "_tick")

	_block_hit_map = BlockDataMap.new()
	_block_hit_map.clear()

	for block in get_children():
		if block is BlockDefault:
			block.parent_map = self
			block.block_position = Vector2(int(block.translation.x), int(block.translation.z))
			block.block_rotation = int(round(wrapf(-block.rotation_degrees.y / 90, 0, 4)))
			block.rotation_degrees.y = block.block_rotation * -90
			_block_hit_map.add_block(block)
		else:
			block.queue_free()


func in_bounds(point: Vector2):
	return bounds.has_point(point)


var _loaded_prefabs = {}


func get_item(point: Vector2):
	return _block_hit_map.get_item(point)


func get_block(point: Vector2):
	return _block_hit_map.get_block(point)


func spawn_item(item_data: Dictionary):
	if _block_hit_map.get_item(Vector2(item_data["x"], item_data["y"])) != null:
		return false

	var prefab_path = item_data["prefab"]
	var prefab = _loaded_prefabs.get(prefab_path)

	if prefab == null:
		prefab = load(prefab_path)
		_loaded_prefabs[prefab_path] = prefab

	var item = prefab.instance()
	item.parent_map = self
	add_child(item)
	item.load_save(item_data)
	_block_hit_map.add_item(item)

	return true


func spawn_block(block_data: Dictionary):
	if _block_hit_map.get_block(Vector2(block_data["x"], block_data["y"])) != null:
		return false

	var prefab_path = block_data["prefab"]
	var prefab = _loaded_prefabs.get(prefab_path)

	if prefab == null:
		prefab = load(prefab_path)
		_loaded_prefabs[prefab_path] = prefab

	var block = prefab.instance()
	block.parent_map = self
	add_child(block)
	block.load_save(block_data)
	_block_hit_map.add_block(block)

	return true


func remove_item(item):
	_block_hit_map.remove_item(item)
	item.queue_free()


func remove_block(block):
	_block_hit_map.remove_block(block)
	block.queue_free()


var _prev_item_pos: Dictionary = {}

const _side_map = [Vector2.UP, Vector2.RIGHT, Vector2.DOWN, Vector2.LEFT]


func push_item(point: Vector2, direction: int):
	var item = _block_hit_map.get_item(point)
	if item == null:
		return true

	if item.was_modified:
		return false

	var to = point + _side_map[direction]

	if not in_bounds(to):
		return false

	var target_block = _block_hit_map.get_block(to)

	# no block to move the item on top of
	if target_block == null:
		return false

	if not target_block.can_accept_item(item, wrapi(direction + 2, 0, 4)):
		return false

	if _prev_item_pos.has(to):
		return false

	return target_block.receive_item(item, wrapi(direction + 2, 0, 4))


var _pre_sim_state: Dictionary = {}


func save():
	var blocks = []

	for block in _block_hit_map._map.values():
		var block_data = block.save()
		blocks.append(block_data)

	return {"blocks": blocks}


func load_save(save_data: Dictionary):
	_block_hit_map.clear()

	for i in get_children():
		i.queue_free()

	for block_data in save_data["blocks"]:
		spawn_block(block_data)


func _on_sim_start():
	_pre_sim_state = save()


func _on_sim_end():
	load_save(_pre_sim_state)


func _tick(delta: float):
	# for node in get_tree().get_nodes_in_group("block"):
	# 	if node is BlockDefault and node.is_pushable():
	# 		push_block(node.block_position, Vector3.DOWN)

	_prev_item_pos.clear()

	get_tree().call_group("block", "_tick")
	get_tree().call_group("item", "_tick")
	get_tree().call_group("block", "_animate", delta)
	get_tree().call_group("item", "_animate", delta)

	for node in get_tree().get_nodes_in_group("block"):
		if node is BlockDefault:
			node.was_modified = false

	for node in get_tree().get_nodes_in_group("item"):
		if node is ItemDefault:
			node.was_modified = false


func _unhandled_input(event):
	var is_mouse_motion = event is InputEventMouseMotion
	var is_mouse_button = not is_mouse_motion and event is InputEventMouseButton

	if _game_state.is_sim_running:
		# _preview_node.visible = false
		pass

	elif is_mouse_motion or is_mouse_button:
		# convert the screen space point to world space
		var point = (
			get_viewport().get_camera().project_ray_origin(event.position)
			- translation
			+ Vector3.ONE * 0.5
		)

		# since we are using an orthographic camera
		# all we need to do is set the y to 0 where the floor is
		point.y = 0

		point = point.floor()

		var is_left_down = false
		var is_right_down = false

		if is_mouse_motion:
			# update the preview mesh position
			# if _preview_node != null and _game_state.can_edit_block(point.x, point.y, point.z):
			# 	_preview_node.translation = point + _map_node.translation
			# 	_preview_node.visible = true
			# else:
			# 	_preview_node.visible = false

			is_left_down = event.button_mask == BUTTON_MASK_LEFT
			is_right_down = event.button_mask == BUTTON_MASK_RIGHT
		elif event.is_pressed():
			is_left_down = event.button_index == BUTTON_LEFT
			is_right_down = event.button_index == BUTTON_RIGHT

		if build_bounds.has_point(Vector2(point.x, point.z)):
			var block = _block_hit_map.get_block(Vector2(point.x, point.z))

			if is_left_down:
				if block != null:
					remove_block(block)

				spawn_block(
					{
						"prefab": _game_state.block_library[_game_state.current_id],
						"x": point.x,
						"y": point.z,
						"r": _game_state.current_orientation
					}
				)
				get_tree().set_input_as_handled()

			elif is_right_down:
				if block != null:
					remove_block(block)
				get_tree().set_input_as_handled()

	elif event is InputEventKey:
		if event.pressed and event.scancode == KEY_Q:
			_game_state.current_orientation = (_game_state.current_orientation + 3) % 4

		if event.pressed and event.scancode == KEY_E:
			_game_state.current_orientation = (_game_state.current_orientation + 1) % 4
