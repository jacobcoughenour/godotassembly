extends ItemList

class_name BlockItemList

var _game_state: GameState

var PrefabPreview = load("res://PrefabPreview.tscn")


func _ready():
	select_mode = SELECT_SINGLE

	_game_state = get_node("/root/GameState")

	_game_state.connect("current_id_changed", self, "_on_current_id_changed")
	_game_state.connect("block_library_changed", self, "_on_block_library_changed")

	connect("item_selected", self, "_on_item_selected")

	_on_block_library_changed([], _game_state.block_library)


func _on_item_selected(id: int):
	_game_state.current_id = id


func _on_current_id_changed(_prev_id: int, id: int):
	if id == -1:
		unselect_all()
	else:
		select(id, true)


func _on_block_library_changed(_prev: Array, library: Array):
	# clear the list first
	clear()
	unselect_all()

	var preview = PrefabPreview.instance()
	_game_state.add_child(preview)

	# populate the list with the items in the block library
	for block_prefab_path in library:
		preview.prefab_path = block_prefab_path

		# wait for the visual server to draw a frame
		yield(VisualServer, "frame_post_draw")

		var img = preview.get_texture().get_data()
		img.flip_y()

		var tex = ImageTexture.new()
		tex.create_from_image(img)

		add_icon_item(tex, true)

	preview.queue_free()

	# reselect the current id
	_on_current_id_changed(-1, _game_state.current_id)
