extends MeshInstance

class_name ChunkInstance

var PlanetData = preload("res://PlanetData.gd")


func clear():
	pass


const HEIGHT_SCALE = 0.5

#   0 - 1
#   |   |
#   3 - 2

const surface_verts = [
	Vector3(0, 0, 0),
	Vector3(1, 0, 0),
	Vector3(1, 0, 1),
	Vector3(0, 0, 1),
]
const surface_uvs = [
	Vector2.ZERO,
	Vector2.RIGHT,
	Vector2.ONE,
	Vector2.DOWN,
]
const surface_indices = [
	[0, 1, 2, 0, 2, 3],
	[0, 1, 3, 1, 2, 3],
]

var _planet_data = null
var _chunk_data = []
var _chunk_pos: Vector2
var _min_pos: Vector2
var _max_pos: Vector2


func render_chunk_at(planet_data: PlanetData, pos: Vector2):
	clear()

	_planet_data = planet_data

	# grab height data for this chunk
	_chunk_pos = pos
	_chunk_data = _planet_data.get_chunk_data(pos)

	transform = Transform(
		Basis(), Vector3(pos.x * PlanetData.CHUNK_SIZE, 0, pos.y * PlanetData.CHUNK_SIZE)
	)

	# top-left tile position
	_min_pos = pos * PlanetData.CHUNK_SIZE
	# bottom-right tile position
	_max_pos = Vector2(
		(pos.x + 1) * PlanetData.CHUNK_SIZE - 1, (pos.y + 1) * PlanetData.CHUNK_SIZE - 1
	)

	# we need to get the chunk data samples from the tiles in adjacent chunks so
	# we can smoothly blend between the chunks

	# get the 4 corner samples
	#   0 - 1
	#   |   |
	#   3 - 2

	# var corner_samples = [
	# 	planet_data.get_tile(min_pos - Vector2.ONE),
	# 	planet_data.get_tile(Vector2(max_pos.x + 1, min_pos.y - 1)),
	# 	planet_data.get_tile(max_pos + Vector2.ONE),
	# 	planet_data.get_tile(Vector2(max_pos.x + 1, min_pos.y - 1)),
	# ]

	# grab adjacent blocks for the other chunks

	# mesh

	var surface_tool = SurfaceTool.new()
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)

	var vert_index = 0

	for z in PlanetData.CHUNK_SIZE:
		for x in PlanetData.CHUNK_SIZE:
			var sample = get_verts(x, z)
			var verts = sample[0]

			for i in 4:
				surface_tool.add_uv(surface_uvs[i])
				surface_tool.add_vertex(verts[i])

			# surface_tool.add_smooth_group(true)

			for index in surface_indices[sample[1]]:
				surface_tool.add_index(index + vert_index)
			vert_index += 4

			# right side

			var right_verts = get_verts(x + 1, z)[0]

			#   0 - 1  1 - 2  0 - 1
			#   |   |  |   |  |   |
			#   3 - 2  0 - 3  3 - 2

			if verts[1].y != right_verts[0].y or verts[2].y != right_verts[3].y:
				surface_tool.add_uv(surface_uvs[0])
				surface_tool.add_vertex(verts[2])
				surface_tool.add_uv(surface_uvs[1])
				surface_tool.add_vertex(verts[1])
				surface_tool.add_uv(surface_uvs[2])
				surface_tool.add_vertex(right_verts[0])
				surface_tool.add_uv(surface_uvs[3])
				surface_tool.add_vertex(right_verts[3])

				for index in surface_indices[0]:
					surface_tool.add_index(index + vert_index)
				vert_index += 4

			# bottom side

			var bottom_verts = get_verts(x, z + 1)[0]

			#   0...1
			#   :   :
			#   3---2
			#   |   |
			#   0---1
			#   :   :
			#   3...2

			if verts[3].y != bottom_verts[0].y or verts[2].y != bottom_verts[1].y:
				surface_tool.add_uv(surface_uvs[0])
				surface_tool.add_vertex(verts[3])
				surface_tool.add_uv(surface_uvs[1])
				surface_tool.add_vertex(verts[2])
				surface_tool.add_uv(surface_uvs[2])
				surface_tool.add_vertex(bottom_verts[1])
				surface_tool.add_uv(surface_uvs[3])
				surface_tool.add_vertex(bottom_verts[0])

				for index in surface_indices[0]:
					surface_tool.add_index(index + vert_index)
				vert_index += 4

	surface_tool.generate_normals()
	surface_tool.generate_tangents()
	mesh = surface_tool.commit()

	create_trimesh_collision()


func get_sample(x: int, z: int):
	if x < 0 or x >= PlanetData.CHUNK_SIZE or z < 0 or z >= PlanetData.CHUNK_SIZE:
		return _planet_data.get_tile(_min_pos.x + x, _min_pos.y + z)

	return _chunk_data[x + z * PlanetData.CHUNK_SIZE]


func get_verts(x: int, z: int):
	var sample = get_sample(x, z)

	var edges = [
		get_sample(x, z - 1) - sample,
		get_sample(x + 1, z) - sample,
		get_sample(x, z + 1) - sample,
		get_sample(x - 1, z) - sample
	]

	var corners = [
		get_sample(x - 1, z - 1) - sample,
		get_sample(x + 1, z - 1) - sample,
		get_sample(x + 1, z + 1) - sample,
		get_sample(x - 1, z + 1) - sample
	]

	var edge_flags = [false, false, false, false]

	var edge_count = 0

	for i in 4:
		if corners[i] > 0 or edges[i] > 0 or edges[wrapi(i - 1, 0, 4)] > 0:
			edge_flags[i] = true
			edge_count += 1

	var verts = []

	for i in 4:
		var point = Vector3(x, sample * HEIGHT_SCALE, z) + surface_verts[i]

		if edge_flags[i]:
			point += Vector3.UP * HEIGHT_SCALE
			if edge_count == 3 and corners[i] > 1:
				point += Vector3.UP * HEIGHT_SCALE

		verts.append(point)

	var index_group = 0

	if (
		edge_count == 1 and (edge_flags[0] or edge_flags[2])
		or edge_count == 3 and (not edge_flags[0] or not edge_flags[2])
	):
		index_group = 1

	return [verts, index_group]

# func create_foliage():
# 	var pos = bounds.position
# 	var end = bounds.end

# 	var index = 0
# 	grass_mesh.multimesh.instance_count = height_map_width * height_map_height

# 	grass_mesh.multimesh.visible_instance_count = 0

# 	for z in range(pos.y, end.y):
# 		for x in range(pos.x, end.x):
# 			var sample = get_sample(x, z)

# 			if sample >= 0:
# 				grass_mesh.multimesh.visible_instance_count = index + 1
# 				grass_mesh.multimesh.set_instance_transform(
# 					index,
# 					Transform(
# 						Basis().scaled(Vector3.ONE * 2),
# 						Vector3(x + 0.5, sample * height_scale, z + 0.5)
# 					)
# 				)

# 				index += 1
