extends Node

# class_name GameState

signal current_id_changed(prev, next)
signal current_orientation_changed(prev, next)
signal block_library_changed(prev, next)
signal sim_start
signal sim_pause
signal sim_end
signal tick(delta)

export var current_id = 0 setget set_current_id
export var current_orientation = 0 setget set_current_orientation

var block_library = [
	"res://blocks/BlockDefault.tscn",
	"res://blocks/BlockConveyor.tscn",
	"res://blocks/BlockPackager.tscn"
]

# todo add tools here


func set_current_id(id: int):
	var prev = current_id
	current_id = id
	emit_signal("current_id_changed", prev, id)


func set_current_orientation(id: int):
	var prev = current_orientation
	current_orientation = id
	emit_signal("current_orientation_changed", prev, id)


var timer: Timer
var is_sim_running = false
var frame = 0
var tick_speed_normal = 0.4
var tick_speed_fast = 0.1


func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.wait_time = tick_speed_normal
	timer.connect("timeout", self, "_tick")


func _input(event):
	if event as InputEventKey:
		if event.scancode == KEY_F:
			if event.pressed:
				timer.wait_time = tick_speed_fast
			else:
				timer.wait_time = tick_speed_normal
		elif event.pressed:
			# if event.scancode == KEY_R:
			# 	if is_sim_running:
			# 		_end_sim()
			# 	else:
			# 		_start_sim()

			if event.scancode == KEY_F11:
				toggle_fullscreen()
			elif event.scancode == KEY_ESCAPE:
				if is_sim_running:
					_end_sim()
				else:
					get_tree().quit()


func toggle_fullscreen():
	OS.window_fullscreen = ! OS.window_fullscreen


func _start_sim():
	frame = 0

	# todo save map here

	timer.start()
	is_sim_running = true
	emit_signal("sim_start")

	_tick()


func _pause_sim():
	timer.paused = true
	emit_signal("sim_pause")


func _end_sim():
	timer.stop()

	# todo restore map

	is_sim_running = false

	emit_signal("sim_end")


func _tick():
	emit_signal("tick", timer.wait_time)
	frame += 1
