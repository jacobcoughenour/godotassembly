extends Object

class_name PlanetData_old

const CHUNK_SIZE = 64

var _chunk_map = {}

var noise = OpenSimplexNoise.new()


func _init():
	noise.seed = 1201
	noise.octaves = 4
	noise.period = 32.0
	noise.persistence = 0.5


func get_tile(x: int, z: int):
	return get_chunk_data(Vector2(x / CHUNK_SIZE, z / CHUNK_SIZE))[(
		wrapi(x, 0, CHUNK_SIZE)
		+ (wrapi(z, 0, CHUNK_SIZE) * CHUNK_SIZE)
	)]


func get_chunk_data(chunk_pos: Vector2):
	if not _chunk_map.has(chunk_pos):
		var chunk_data = generate_chunk(int(chunk_pos.x), int(chunk_pos.y))
		_chunk_map[chunk_pos] = chunk_data
		return chunk_data
	return _chunk_map[chunk_pos]


func generate_chunk(c_x: int, c_z: int):
	var height_map = []
	height_map.resize(CHUNK_SIZE * CHUNK_SIZE)
	var index = 0

	for z in CHUNK_SIZE:
		for x in CHUNK_SIZE:
			var sample = noise.get_noise_2d(x - c_x, z - c_z) * 5
			height_map[index] = int(max(sign(sample) * abs(pow(sample, 3)), -2))
			index += 1

	_chunk_map[Vector2(c_x, c_z)] = height_map
	return height_map
