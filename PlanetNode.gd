tool
extends PlanetNode

export (NodePath) var sun_light_path = "SunLight" setget _set_sun_light_path
onready var sun_light_node: DirectionalLight = get_node(sun_light_path)

export (NodePath) var environment_path = "PlanetEnvironment" setget _set_environment_path
onready var environment_node: WorldEnvironment = get_node(environment_path)

# export (Color) var sun_light_color


func _set_sun_light_path(value: NodePath):
	sun_light_path = value
	sun_light_node = get_node(sun_light_path)


func _set_environment_path(value: NodePath):
	environment_path = value
	environment_node = get_node(environment_path)


export (Gradient) var light_color_gradient setget _set_light_gradient
export (Gradient) var ambient_color_gradient setget _set_ambient_gradient
export (int, 0, 24000) var time = 0 setget _set_time
export (float) var time_scale = 1


func _ready():
	pass


func _process(delta: float):
	if not Engine.editor_hint:
		time += delta * time_scale
		_update_time()


func _set_time(value: int):
	time = value
	_update_time()


func _set_light_gradient(value: Gradient):
	light_color_gradient = value
	_update_time()


func _set_ambient_gradient(value: Gradient):
	ambient_color_gradient = value
	_update_time()


func _update_time():
	if light_color_gradient == null or sun_light_node == null:
		return

	var light_color = light_color_gradient.interpolate(time / 24000.0)
	sun_light_node.light_color = light_color
	sun_light_node.light_energy = light_color.a

	sun_light_node.rotation.x = (wrapf(wrapf((time / 12000.0) + 0.5, 0.0, 2.0), 0.0, 1.0) + 1) * PI
