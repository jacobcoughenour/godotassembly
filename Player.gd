extends KinematicBody

class_name Player

const GRAVITY = -8
export var MOVE_SPEED = 8
var velocity = Vector3()
export var freeze_movement = true

export (NodePath) var camera_arm_path = "CameraArm"
export (NodePath) var camera_path = "CameraArm/Camera"
export (NodePath) var tween_path = "Tween"

onready var camera_arm: Spatial = get_node(camera_arm_path)
onready var camera: Camera = get_node(camera_path)
onready var tween: Tween = get_node(tween_path)

export var CAMERA_ZOOM_STEPS = 16
# export var CAMERA_ZOOM_SPEED = 8
export (int) var camera_zoom = 0 setget _set_camera_zoom


func _set_camera_zoom(value: int):
	camera_zoom = value
	_update_camera_zoom()


export var CAMERA_MIN_DISTANCE = 8
export var CAMERA_MAX_DISTANCE = 1024
export var CAMERA_MIN_ANGLE = -60
export var CAMERA_MAX_ANGLE = -90
export var CAMERA_MIN_FOV = 40
export var CAMERA_MAX_FOV = 10


func _ready():
	_update_camera_zoom()
	pass


func _physics_process(delta):
	if freeze_movement:
		velocity = Vector3.ZERO
		return

	var input_dir = Vector2()

	if Input.is_action_pressed("move_forward"):
		input_dir += Vector2.UP
	if Input.is_action_pressed("move_back"):
		input_dir += Vector2.DOWN
	if Input.is_action_pressed("move_right"):
		input_dir += Vector2.RIGHT
	if Input.is_action_pressed("move_left"):
		input_dir += Vector2.LEFT

	input_dir = input_dir.normalized() * MOVE_SPEED

	if is_on_floor():
		velocity.y = 0
		velocity.x = input_dir.x
		velocity.z = input_dir.y
	else:
		velocity.x = 0
		velocity.y += delta * GRAVITY
		velocity.z = 0

	velocity = move_and_slide_with_snap(velocity, Vector3.DOWN, Vector3.UP, true, 4, PI * 0.4)


func _input(event):
	if event is InputEventMouseButton and event.is_pressed():
		var delta = 0
		if event.button_index == BUTTON_WHEEL_UP:
			delta += -1
		elif event.button_index == BUTTON_WHEEL_DOWN:
			delta += 1

		camera_zoom = clamp(camera_zoom + delta, 0, CAMERA_ZOOM_STEPS)
		_update_camera_zoom()


func _update_camera_zoom():
	var ratio = pow(camera_zoom / float(CAMERA_ZOOM_STEPS), 1)

	var dist = lerp(CAMERA_MIN_DISTANCE, CAMERA_MAX_DISTANCE, pow(ratio, 2))
	# camera.translation.z = dist

	var duration = 0.09
	var trans_type = Tween.TRANS_LINEAR
	var ease_type = Tween.EASE_OUT

	tween.stop_all()
	tween.interpolate_property(
		camera, "translation:z", camera.translation.z, dist, duration, trans_type, ease_type
	)
	tween.interpolate_property(
		camera, "far", camera.far, dist + 64, duration, trans_type, ease_type
	)
	tween.interpolate_property(
		camera, "near", camera.near, max(dist - 64, 0.05), duration, trans_type, ease_type
	)
	tween.interpolate_property(
		camera,
		"fov",
		camera.fov,
		lerp(CAMERA_MIN_FOV, CAMERA_MAX_FOV, pow(clamp(ratio, 0, 1), 1)),
		duration,
		trans_type,
		ease_type
	)
	tween.interpolate_property(
		camera_arm,
		"rotation:x",
		camera_arm.rotation.x,
		deg2rad(lerp(CAMERA_MIN_ANGLE, CAMERA_MAX_ANGLE, pow(clamp(ratio * 2 - 1, 0, 1), 0.5))),
		duration,
		trans_type,
		ease_type
	)
	tween.start()

	# camera.far = dist + 64
	# camera.near = max(dist - 64, 0.05)
	# camera.fov = lerp(CAMERA_MIN_FOV, CAMERA_MAX_FOV, pow(ratio, 0.25))
	# camera_arm.rotation.x = deg2rad(lerp(CAMERA_MIN_ANGLE, CAMERA_MAX_ANGLE, pow(ratio, 1)))
