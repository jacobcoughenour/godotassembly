extends Viewport

class_name PrefabPreview

export (NodePath) var slot_path

export (String, FILE, "*.tscn") var prefab_path setget _set_prefab_path

onready var _slot: Spatial = get_node(slot_path)


func _set_prefab_path(value: String):
	prefab_path = value
	_load_prefab()


func _ready():
	_load_prefab()


func _load_prefab():
	if _slot == null:
		return

	var packed_prefab = load(prefab_path)

	for child in _slot.get_children():
		child.queue_free()

	if packed_prefab == null:
		return

	_slot.add_child(packed_prefab.instance())
