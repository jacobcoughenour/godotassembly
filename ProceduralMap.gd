extends Spatial

class_name ProceduralMap

export (NodePath) var player_path = "../Player" setget _set_player_path
onready var player_node: Player = get_node(player_path)

var ChunkInstance = load("res://ChunkInstance.tscn")


func _set_player_path(value: NodePath):
	player_path = value
	player_node = get_node(value)


var planet_data = PlanetData.new()


func _ready():
	update_chunks_to_show()


var time_since_last_chunk_update = 0


func _process(_delta):
	time_since_last_chunk_update += _delta

	if time_since_last_chunk_update > 0.5:
		update_chunks_to_show()
		time_since_last_chunk_update = 0


var chunk_nodes = {}

var last_camera_position = Vector3(0, -10000, 0)


func update_chunks_to_show():
	if player_node == null:
		return

	var camera_pos = player_node.translation

	if camera_pos.distance_squared_to(last_camera_position) < 4:
		return

	last_camera_position = camera_pos

	for z in range(-2, 2):
		for x in range(-2, 2):
			call_deferred(
				"show_chunk",
				Vector2(
					floor(camera_pos.x / PlanetData.CHUNK_SIZE) + x,
					floor(camera_pos.z / PlanetData.CHUNK_SIZE) + z
				)
			)


func show_chunk(pos: Vector2):
	print("show_chunk ", pos)

	if chunk_nodes.has(pos):
		chunk_nodes[pos].visible = true
		return

	var chunk = ChunkInstance.instance()
	add_child(chunk)
	chunk_nodes[pos] = chunk
	chunk.render_chunk_at(planet_data, pos)


func hide_chunk(pos: Vector2):
	if chunk_nodes.has(pos):
		chunk_nodes[pos].visible = false


func unload_chunk(pos: Vector2):
	if chunk_nodes.has(pos):
		chunk_nodes[pos].queue_free()
		chunk_nodes.erase(pos)
