extends BlockDefault

class_name BlockConsumer

export (Array, String, FILE, "*.tscn") var allowed_item_prefabs


func _tick():
	var item = parent_map.get_item(block_position)

	if item != null and not item.was_modified:
		parent_map.remove_item(item)
