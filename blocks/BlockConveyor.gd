extends BlockDefault

class_name BlockConveyor


func _tick():
	parent_map.push_item(block_position, block_rotation)


func can_accept_item(_item, side: int):
	return wrapi(side - block_rotation, 0, 4) != 0
