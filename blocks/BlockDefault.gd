extends Spatial

class_name BlockDefault

var AssemblyMap = preload("res://AssemblyMap.gd")
var parent_map: AssemblyMap setget _set_parent_map


func _set_parent_map(value: AssemblyMap):
	parent_map = value
	if parent_map == null:
		remove_from_group("block")
	else:
		add_to_group("block")


export (String, FILE, "*.tscn") var prefab_path

var block_position: Vector2
var block_rotation: int
var was_modified: bool = false


func _ready():
	pass


func save() -> Dictionary:
	return {
		"prefab": prefab_path, "x": block_position.x, "y": block_position.y, "r": block_rotation
	}


func load_save(save_data: Dictionary) -> void:
	block_position = Vector2(int(save_data["x"]), int(save_data["y"]))
	block_rotation = wrapi(save_data["r"], 0, 4)
	translation = Vector3(block_position.x, 0, block_position.y)
	rotation_degrees.y = wrapi(block_rotation, 0, 4) * -90


func _get_occupied_points() -> Array:
	# todo add any child block positions to this
	return [block_position]


func can_accept_item(_item: ItemDefault, _side: int) -> bool:
	return true


func receive_item(item: ItemDefault, side: int) -> bool:
	# try pushing the item on this block if there is any
	if parent_map.push_item(block_position, wrapi(side + 2, 0, 4)):
		item.was_modified = true
		parent_map._prev_item_pos[item.item_position] = item
		parent_map._block_hit_map.remove_item(item)
		item.item_position = block_position
		parent_map._block_hit_map.add_item(item)
		return true

	return false


func _tick() -> void:
	pass


func _animate(_delta: float) -> void:
	translation = Vector3(block_position.x, 0, block_position.y)
	rotation_degrees.y = wrapi(block_rotation, 0, 4) * -90
