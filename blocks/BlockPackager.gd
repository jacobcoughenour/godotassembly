extends BlockConveyor

class_name BlockPackager

var ItemContainer = preload("res://items/ItemContainer.gd")


func _tick():
	var cur_container = parent_map.get_item(block_position) as ItemContainer

	if cur_container != null and cur_container.is_full():
		parent_map.push_item(block_position, block_rotation)


func can_accept_item(item, side: int):
	# block items from entering the output side
	if wrapi(side - block_rotation, 0, 4) == 0:
		return false

	# get the current item container on this block
	var cur_container = parent_map.get_item(block_position)

	# if there is not an item container on this block
	# accept an item container
	if cur_container == null and item is ItemContainer:
		return true

	return (
		cur_container != null
		and cur_container is ItemContainer
		and cur_container.can_accept_item(item)
	)


func receive_item(item, side: int):
	if item is ItemContainer:
		return .receive_item(item, side)

	# get the current item container on this block
	var cur_container = parent_map.get_item(block_position) as ItemContainer

	if cur_container.can_accept_item(item):
		item.was_modified = true
		parent_map._prev_item_pos[item.item_position] = item
		parent_map._block_hit_map.remove_item(item)
		item.item_position = block_position

		cur_container.add_item(item)

		# item.queue_free()

		# parent_map._block_hit_map.add_item(item)

		return true
	return false
