extends BlockConveyor

class_name BlockSpawner

enum SpawnMethod { ROUND_ROBIN, RANDOM }

export (Array, String, FILE, "*.tscn") var item_prefabs
export (int, 0, 999) var spawn_interval = 0
export (SpawnMethod) var spawn_method = SpawnMethod.ROUND_ROBIN

var time_until_spawn = 0
var round_robin_index = 0


func save():
	var data = .save()
	data["item_prefabs"] = item_prefabs
	data["spawn_interval"] = spawn_interval
	data["spawn_method"] = spawn_method
	return data


func load_save(save_data: Dictionary):
	.load_save(save_data)
	if save_data.has("item_prefabs"):
		item_prefabs = save_data["item_prefabs"]
	if save_data.has("spawn_interval"):
		spawn_interval = save_data["spawn_interval"]
	if save_data.has("spawn_method"):
		spawn_method = save_data["spawn_method"]

	time_until_spawn = 0


func _tick():
	var prefab_len = len(item_prefabs)

	if prefab_len == 0:
		return
	._tick()

	if time_until_spawn <= 0:
		var item_index = (
			round_robin_index
			if spawn_method == SpawnMethod.ROUND_ROBIN
			else randi() % prefab_len
		)

		if parent_map.spawn_item(
			{
				"prefab": item_prefabs[item_index],
				"x": block_position.x,
				"y": block_position.y,
				"r": block_rotation
			}
		):
			time_until_spawn = spawn_interval - 1
			round_robin_index = (round_robin_index + 1) % prefab_len

	time_until_spawn -= 1
