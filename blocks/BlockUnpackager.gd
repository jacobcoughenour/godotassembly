extends BlockConveyor

class_name BlockUnpackager

var ItemContainer = preload("res://items/ItemContainer.gd")

var lock_for_a_tick = false


func _tick():
	var cur_container = parent_map.get_item(block_position) as ItemContainer

	if cur_container == null:
		lock_for_a_tick = false
		return

	if lock_for_a_tick == false:
		lock_for_a_tick = true
		return

	if cur_container.is_empty():
		parent_map.push_item(block_position, block_rotation)
		return

	var output_pos = block_position + parent_map._side_map[block_rotation]

	if parent_map.push_item(output_pos, block_rotation):
		var item = cur_container.pop_item()

		# item.translation = Vector3(block_position.x, 1, block_position.y)
		# item.rotation_degrees.y = wrapi(item.item_rotation, 0, 4) * -90
		item.item_position = output_pos
		parent_map._block_hit_map.add_item(item)
		item.was_modified = true


func can_accept_item(item, side: int):
	# block items from entering the output side
	if wrapi(side - block_rotation, 0, 4) == 0:
		return false

	# get the current item container on this block
	var cur_container = parent_map.get_item(block_position)

	# if there is not an item container on this block
	# accept an item container
	return cur_container == null and item is ItemContainer


func receive_item(item, side: int):
	if item is ItemContainer:
		return .receive_item(item, side)
	return false
