extends ItemDefault

class_name ItemContainer
var ItemContainer = load("res://items/ItemContainer.gd")

enum PackingOrder { QUEUE, STACK, RANDOM }

export (int, 0, 32) var capacity = 0
export (PackingOrder) var packing_order = PackingOrder.QUEUE

var contents: Array = []


func _ready():
	pass


func can_accept_item(item: ItemDefault):
	if item is ItemContainer:
		return false
	return not is_full()


func add_item(item: ItemDefault):
	var index = len(contents)
	contents.append(item)

	parent_map.remove_child(item)

	var slot = get_node("Slot%d" % index)

	assert(slot != null, "missing position3d node for slot %d" % index)

	slot.add_child(item)

	item.translation = Vector3.ZERO
	item.rotation = Vector3.ZERO

	item.set_owner(self)
	item.item_parent_container = self


func pop_item():
	var item: ItemDefault = null

	match packing_order:
		PackingOrder.QUEUE:
			item = contents.pop_front()
		PackingOrder.STACK:
			item = contents.pop_back()
		PackingOrder.RANDOM:
			var index = randi() % len(contents)
			item = contents[index]
			contents.remove(index)

	var slot = item.get_parent()
	slot.remove_child(item)

	item.item_parent_container = null
	parent_map.add_child(item)
	item.set_owner(parent_map)

	item.global_transform = slot.global_transform

	return item


func is_full():
	return len(contents) == capacity


func is_empty():
	return len(contents) == 0
