extends Resource

class_name ItemData

export (String, FILE, "*.tscn") var prefab
export var x: int
export var y: int
export (int, 4) var r


func _init(p_prefab = "", p_x: int = 0, p_y: int = 0, p_r: int = 0):
	prefab = p_prefab
	x = p_x
	y = p_y
	r = p_r
