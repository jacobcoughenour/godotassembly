extends Spatial

class_name ItemDefault

var AssemblyMap = preload("res://AssemblyMap.gd")
var parent_map: AssemblyMap setget _set_parent_map


func _set_parent_map(value: AssemblyMap):
	parent_map = value
	remove_from_group("item")
	if parent_map != null:
		add_to_group("item")


export (String, FILE, "*.tscn") var prefab_path = "res://items/ItemDefault.tscn"

var item_position: Vector2
var item_rotation: int
var was_modified: bool = false

var item_parent_container: ItemDefault = null
var _last_parent_container: ItemDefault = null

var tween: Tween


func _ready():
	tween = Tween.new()
	add_child(tween)


func save():
	return {"prefab": prefab_path, "x": item_position.x, "y": item_position.y, "r": item_rotation}


func load_save(save_data: Dictionary):
	item_position = Vector2(int(save_data["x"]), int(save_data["y"]))
	item_rotation = wrapi(save_data["r"], 0, 4)
	translation = Vector3(item_position.x, 1, item_position.y)
	rotation_degrees.y = wrapi(item_rotation, 0, 4) * -90


func _tick():
	pass


func _animate(delta: float):
	if item_parent_container != null:
		return

	tween.stop_all()
	tween.interpolate_property(
		self,
		"translation",
		translation,
		Vector3(item_position.x, 1, item_position.y),
		delta,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		self,
		"rotation_degrees:y",
		rotation_degrees.y,
		wrapi(item_rotation, 0, 4) * -90,
		delta,
		Tween.TRANS_EXPO,
		Tween.EASE_IN_OUT
	)
	tween.interpolate_property(
		self,
		"scale",
		scale,
		Vector3.ONE,
		delta * 0.8,
		Tween.TRANS_ELASTIC,
		Tween.EASE_OUT,
		delta * 0.3
	)
	tween.start()

	_last_parent_container = item_parent_container

	# translation = Vector3(item_position.x, 1, item_position.y)
	# rotation_degrees.y = wrapi(item_rotation, 0, 4) * -90
