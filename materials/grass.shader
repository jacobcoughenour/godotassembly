shader_type spatial;
render_mode depth_draw_alpha_prepass;


varying vec4 color;
varying vec3 normal;

uniform float blend_softness_0:hint_range(0.01, 1.0) = 0.025;
uniform float blend_softness_1:hint_range(0.01, 1.0) = 0.025;
uniform float blend_softness_2:hint_range(0.01, 1.0) = 0.025;


uniform vec4 color_0:hint_color = vec4(1, 1, 1, 1);
uniform float uv_scale_0:hint_range(0.01, 100.0) = 1;
uniform sampler2D albedo_0;
uniform sampler2D height_0;
uniform sampler2D ao_0;
uniform sampler2D metallic_0;
uniform sampler2D normal_0;
uniform sampler2D roughness_0;

uniform vec4 color_1:hint_color = vec4(1, 1, 1, 1);
uniform float uv_scale_1:hint_range(0.01, 100.0) = 1;
uniform sampler2D albedo_1;
uniform sampler2D height_1;
uniform sampler2D ao_1;
uniform sampler2D metallic_1;
uniform sampler2D normal_1;
uniform sampler2D roughness_1;

uniform vec4 color_2:hint_color = vec4(1, 1, 1, 1);
uniform float uv_scale_2:hint_range(0.01, 100.0) = 1;
uniform sampler2D albedo_2;
uniform sampler2D height_2;
uniform sampler2D ao_2;
uniform sampler2D metallic_2;
uniform sampler2D normal_2;
uniform sampler2D roughness_2;

uniform vec4 color_3:hint_color = vec4(1, 1, 1, 1);
uniform float uv_scale_3:hint_range(0.01, 100.0) = 1;
uniform sampler2D albedo_3;
uniform sampler2D height_3;
uniform sampler2D ao_3;
uniform sampler2D metallic_3;
uniform sampler2D normal_3;
uniform sampler2D roughness_3;


//vec2 random2(vec2 p) {
//	return fract(sin(vec2(
//		dot(p, vec2(127.32, 231.4)),
//		dot(p, vec2(12.3, 146.3))
//	)) * 231.23);
//}
//
//float worley2(vec2 p) {
//	float dist = 1.0;
//
//	vec2 i_p = floor(p);
//	vec2 f_p = fract(p);
//
//	for (float y = -1.f; y < 2.f; y++) {
//		for (float x = -1.f; x < 2.f; x++) {
//			vec2 n = vec2(x, y);
//			vec2 diff = n + random2(i_p + n) - f_p;
//			dist = min(dist, diff.x * diff.x + diff.y * diff.y);
//		}		
//	}
//
//	return sqrt(dist);
//}

vec3 heightblend(vec3 input1, float height1, vec3 input2, float height2, float softness){
	float height_start = max(height1, height2) - softness;
	float level1 = max(height1 - height_start, 0);
	float level2 = max(height2 - height_start, 0);

	return ((input1 * level1) + (input2 * level2)) / (level1 + level2);
}

vec3 heightlerp(vec3 input1, float height1, vec3 input2, float height2, float softness, float t){
	t = clamp(t, 0.0 , 1.0);
    return heightblend(input1, height1 * (1.0 - t), input2, height2 * t, softness);
}

vec4 heightblend4(vec4 input1, float height1, vec4 input2, float height2, float softness){
	float height_start = max(height1, height2) - softness;
	float level1 = max(height1 - height_start, 0);
	float level2 = max(height2 - height_start, 0);

	return ((input1 * level1) + (input2 * level2)) / (level1 + level2);
}

vec4 heightlerp4(vec4 input1, float height1, vec4 input2, float height2, float softness, float t){
	t = clamp(t, 0.0 , 1.0);
    return heightblend4(input1, height1 * (1.0 - t), input2, height2 * t, softness);
}

void vertex() {
	color = COLOR;
	normal = NORMAL;
}

void fragment() {
	vec4 world_position = CAMERA_MATRIX * vec4(VERTEX, 1.0);
	
	vec2 uv_x = world_position.yz;
	vec2 uv_y = world_position.xz;
	vec2 uv_z = world_position.xy;
	
	vec2 uv_0 = uv_y * uv_scale_0;
	vec3 col_0 = texture(albedo_0, uv_0).rgb * color_0.rgb;
	vec4 mat_0 = vec4(
		texture(height_0, uv_0).r,
		texture(ao_0, uv_0).r,
		texture(metallic_0, uv_0).r,
		texture(roughness_0, uv_0).r
	);
	vec3 nor_0 = texture(normal_0, uv_0).rgb;
	
	vec2 uv_1 = uv_y * uv_scale_1;
	vec3 col_1 = texture(albedo_1, uv_1).rgb * color_1.rgb;
	vec4 mat_1 = vec4(
		texture(height_1, uv_1).r,
		texture(ao_1, uv_1).r,
		texture(metallic_1, uv_1).r,
		texture(roughness_1, uv_1).r
	);
	vec3 nor_1 = texture(normal_1, uv_1).rgb;
	
	vec2 uv_2 = uv_y * uv_scale_2;
	vec3 col_2 = texture(albedo_2, uv_2).rgb * color_2.rgb;
	vec4 mat_2 = vec4(
		texture(height_2, uv_2).r,
		texture(ao_2, uv_2).r,
		texture(metallic_2, uv_2).r,
		texture(roughness_2, uv_2).r
	);
	vec3 nor_2 = texture(normal_2, uv_2).rgb;

	vec2 uv_3 = uv_y * uv_scale_3;
	vec3 col_3 = texture(albedo_3, uv_3).rgb * color_3.rgb;
	vec4 mat_3 = vec4(
		texture(height_3, uv_3).r,
		texture(ao_3, uv_3).r,
		texture(metallic_3, uv_3).r,
		texture(roughness_3, uv_3).r
	);
	vec3 nor_3 = texture(normal_3, uv_3).rgb;
	
	
	vec4 blend = color;
	
	vec3 col_blend = heightlerp(col_0.rgb, mat_0.r, col_1.rgb, mat_1.r, blend_softness_0, blend.r);
	vec3 nor_blend = heightlerp(nor_0, mat_0.r, nor_1, mat_1.r, blend_softness_0, blend.r);
	vec4 mat_blend = heightlerp4(mat_0, mat_0.r, mat_1, mat_1.r, blend_softness_0, blend.r);
	
	col_blend = heightlerp(col_blend, mat_blend.r, col_2.rgb, mat_2.r, blend_softness_1, blend.g);
	nor_blend = heightlerp(nor_blend, mat_blend.r, nor_2, mat_2.r, blend_softness_1, blend.g);
	mat_blend = heightlerp4(mat_blend, mat_blend.r, mat_2, mat_2.r, blend_softness_1, blend.g);
	
	col_blend = heightlerp(col_blend, mat_blend.r, col_3.rgb, mat_3.r, blend_softness_2, blend.b);
	nor_blend = heightlerp(nor_blend, mat_blend.r, nor_3, mat_3.r, blend_softness_2, blend.b);
	mat_blend = heightlerp4(mat_blend, mat_blend.r, mat_3, mat_3.r, blend_softness_2, blend.b);
	
	
	ALBEDO = col_blend;
//	ALBEDO = COLOR.rgb;
	NORMALMAP = nor_blend;
	AO = mat_blend.g;
	METALLIC = mat_blend.b;
	ROUGHNESS = mat_blend.a;
	
	// ALBEDO = vec3(worley2((world_position.xz * 0.8) + (TIME * vec2(0.2, 0.5))));
}