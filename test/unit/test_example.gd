extends "res://addons/gut/test.gd"


class TestBlockDefault:
	extends 'res://addons/gut/test.gd'

	# var BlockDefault = load('res://blocks/BlockDefault.gd')
	var BlockDefaultScene = load('res://blocks/BlockDefault.tscn')
	var _obj = null

	func before_each():
		_obj = BlockDefaultScene.instance()
		add_child(_obj)

	func test_something():
		print(_obj.name)
		print(_obj.get_groups())
		assert_true(_obj.target_position == Vector3.ZERO, "is zero")

	func after_each():
		remove_child(_obj)
		_obj.free()

# class TestAssemblyMap:
# 	extends 'res://addons/gut/test.gd'

# 	var Map = load('res://AssemblyMap.gd')
# 	var _obj = null

# 	func before_each():
# 		_obj = Map.new()

# 	func test_something():
# 		assert_true(_obj._test != 0, "not null")
